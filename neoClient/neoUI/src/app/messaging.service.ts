import { Injectable } from '@angular/core';
import { Message } from './Message';

@Injectable({
  providedIn: 'root'
})
export class MessagingService {
	messages: Message[] = [];

  constructor() { }

  publishMessage(error: any){
  	console.log('full error '+error);
  	console.log('error '+error.error);
  	console.log('message '+error.message);
  	let msg = new Message(error.error, error.message);
  	this.messages.push(msg);
  }

  clear(): void{
  	this.messages = [];
  }


}
