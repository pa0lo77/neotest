import { Component, ViewChild } from '@angular/core';
import { Employee } from './Employee';
import { EmployeesService } from './employees.service'
import { MessagingService } from './messaging.service';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})

export class AppComponent {
	constructor(private empSvc: EmployeesService, private msgSvc: MessagingService){}

  	title = 'neoUI';
  	id: number;
  	name: string;
  	employees: Employee[];
  	empCount: number;
  	@ViewChild('nodeForm', null) empForm;

  	ngOnInit() {
  		this.msgSvc.clear();
  		this.GetEmployees();
  	}



	AddEmployee(): void{
		this.empSvc.AddEmployee(this.id, this.name)
		.subscribe(
			data =>{
			var emp: Employee = new Employee();
			emp.emp_id = data.emp_id;
			emp.name = data.name;
			if(emp.emp_id)
			{
				this.employees.push(emp);
				this.empCount = this.employees.length;
			}
			this.empForm.resetForm();

			},
			error => { console.log('err: '+ error)}
		);
	}

	GetEmployees(): void{
		this.empSvc.GetEmployees()
		.subscribe(
			n=>{
			console.log(n);
			this.employees = n;
			this.empCount = this.employees.length;
			},
			error => {console.log('err:'+error)}
		);
	}

}