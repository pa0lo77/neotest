# Paolo's neo4j Test
A simple API to communicate with and return data from neo4j, and an Angular UI client to test the API.

API Instructions
---------------------- 

The API is a .Net solution. You will need Visual Studio to build it and deploy it on a server, or deply directly to Azure.
The neoApi.dll can be found in neoApi/obj/Release.
An instance of this API is hosted in Azure at: https://paoloneotest.azurewebsites.net/api/

The API exposes 2 RESTful methods:

1. AddEmployee
-----------
Description: Creates an employee node with emp_id and name attributes.

URL: /Employee

URL Params: id = integer, name = string

Method: POST

Success response = Employee object

Error response: HttpStatusCode

Sample request: http://paoloneotest.azurewebsites.net/api/Employee/AddEmployee?id=2&name=Freddie

Sample response: {"emp_id":2,"name":"Freddie"}


2. GetEmployees
------------
Description: Gets all employee nodes from the database.

URL: /Employee

URL Params: (n/a)

Method: GET

Success response = List of Employee objects

Error response: HttpStatusCode

Sample request: https://paoloneotest.azurewebsites.net/api/Employee/GetEmployees


Employee Object
-----------
The employee object has 2 properties:

emp_id = int

name = string

    
Angular UI Instructions
------------------------

The UI client is a single page app developed with Angular 4. An instance of this UI is hosted in Azure at: https://neoclient.azurewebsites.net/

Using NPM build with: ng build --prod

The contents of the build will be available in neoClient/neoUI/dist

Copy the contents of the dist folder to a web server. 