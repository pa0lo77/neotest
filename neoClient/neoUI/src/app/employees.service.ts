import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders, HttpParams } from "@angular/common/http";
import { Observable, of, BehaviorSubject } from "rxjs"; 
import { catchError, map, tap} from "rxjs/operators";
import { environment } from '../environments/environment';
import { Employee } from './Employee';
import { MessagingService } from './messaging.service';

@Injectable({
  providedIn: 'root'
})
export class EmployeesService {

  private api_url = environment.base_url + "Employee/";

  constructor(private http: HttpClient, private msgSvc: MessagingService) { }

  AddEmployee(id: number, name: string): Observable<any>{
  		return this.http.post<any>(this.api_url+"AddEmployee"+"?id="+id+"&name="+name, null)
			.pipe(
				catchError(this.handleError("AddEmployee", []))
				);
  	}

  GetEmployees(): Observable<Employee[]> {
    return this.http.get<Employee[]>(this.api_url+"GetEmployees")
      .pipe(
        catchError(this.handleError("GetEmployees", []))
        );
  }

  private handleError<T>(operation = "operation", result?: T){
		return (error:any): Observable<T> => {
			this.msgSvc.publishMessage(error);
			return of(result as T);
		};
	}


}
