﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace neoApi.Models
{
    public class Employee
    {
        public int emp_id { get; set; }
        public string name { get; set; }

        public Employee(int id, string emp_name)
        {
            emp_id = id;
            name = emp_name;
        }
    }
}