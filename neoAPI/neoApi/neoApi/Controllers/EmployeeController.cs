﻿using Neo4j.Driver.V1;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Threading.Tasks;
using System.Web.Http;
using System.Web.Http.Cors;
using neoApi.Models;
using System.Configuration;

namespace neoApi.Controllers
{
    [EnableCors("*", "*", "GET, POST")]
    public class EmployeeController : ApiController
    {
        private IDriver _neoDriver;
        private string username;
        private string uri;
        private string pwd;

        public EmployeeController()
        {
            try
            {
                this.uri = ConfigurationManager.AppSettings.Get("boltUri").ToString();
                this.pwd = ConfigurationManager.AppSettings.Get("boltPwd").ToString();
                this.username = ConfigurationManager.AppSettings.Get("boltUsername").ToString();
            }
            catch(Exception ex)
            {
                throw ex;
            }

        }

        /// <summary>
        /// Creates a new Employee node
        /// </summary>
        /// <param name="id">Value to use in the node's emp_id attribute</param>
        /// <param name="name">Value to use in the node's name attribute</param>
        /// <returns>Employee object</returns>
        [ActionName("AddEmployee")]
        public IHttpActionResult Post(int id, string name)
        {
            try
            {
                _neoDriver = GraphDatabase.Driver(uri, AuthTokens.Basic(username, pwd));
                using (var session = _neoDriver.Session())
                {
                    session.WriteTransaction(tx =>
                    {
                        tx.Run("create (e:Employee)"+                                "set e.emp_id = "+id+","+                                "e.name = '"+name+"'");
                    });
                }

                return Ok(new Employee(id, name));
            }
            catch(Exception ex)
            {
                return Content(HttpStatusCode.Conflict, "We are unable to communicate with the graph database; "+ex.Message);
            }
            
        }

        /// <summary>
        /// Gets all Employee nodes from the graph database
        /// </summary>
        /// <returns>List of Employee nodes</returns>
        [ActionName("GetEmployees")]
        public IHttpActionResult Get()
        {
            try
            {               
                return Ok(GetEmployees());
            }
            catch (Exception ex)
            {
                return Content(HttpStatusCode.Conflict, ex.Message);
            }

        }

        /// <summary>
        /// Implements the connection to the database to create the Employee list
        /// </summary>
        /// <returns>List of Employees</returns>
        private List<Employee> GetEmployees()
        {
            try
            {
                _neoDriver = GraphDatabase.Driver(uri, AuthTokens.Basic(username, pwd));

                List<Employee> emps = new List<Employee>();
                using (var session = _neoDriver.Session())
                {

                    var employees = session.Run("Match (e:Employee) return e.emp_id as emp_id, e.name as name order by e.emp_id").ToList();

                    foreach (IRecord s in employees)
                    {
                        emps.Add(new Employee(Convert.ToInt32(s["emp_id"]), s["name"].ToString()));
                    }
                    return emps;

                }
            }
            catch(OverflowException)
            {
                throw new Exception("Invalid data value for the employee id attribute.");
            }
            catch(InvalidCastException)
            {
                throw new Exception("Invalid data format for the employee id attribute.");
            }
            catch(FormatException)
            {
                throw new Exception("Invalid data format for the employee id attribute.");
            }
            catch (Exception ex)
            {
                throw new Exception("We are unable to communicate with the graph database; "+ex.Message);
            }
        }
    }
}
