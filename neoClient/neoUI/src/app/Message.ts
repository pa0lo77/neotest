export class Message{
	Error: string;
    Message: string;

  constructor(err: string, msg: string){
    this.Error = err;
    this.Message = msg;
  }
}